package whphw.com.equacao.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoubleUtil {
	/**
	 * Converte para duas casas decimais.
	 * @param decimal
	 * @return
	 */
	public static double decimalPlaces(double decimal, int places) {

		String value = String.valueOf(decimal);
		
		int dotIndex = value.indexOf(".");
		int decimalSize = value.substring(++dotIndex).length();
		boolean isGreatThat = decimalSize > (places + 2);
		int indexEnd = isGreatThat ? value.indexOf(".") + places + 2 : value.indexOf(".") + decimalSize;
		decimal = Double.valueOf(value.substring(0, indexEnd));
		
		return BigDecimal.valueOf(decimal).setScale(places, RoundingMode.HALF_DOWN).doubleValue();
	}
}
