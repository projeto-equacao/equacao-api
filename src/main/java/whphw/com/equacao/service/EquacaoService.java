package whphw.com.equacao.service;

import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

import whphw.com.equacao.dto.request.EquacaoRequest;
import whphw.com.equacao.dto.request.InsertEquacaoRequest;
import whphw.com.equacao.dto.request.UpdateEquacaoRequest;
import whphw.com.equacao.dto.response.EquacaoResponse;
import whphw.com.equacao.entity.EquacaoEntity;
import whphw.com.equacao.repository.jdbc.EquacaoRepository;
import whphw.com.equacao.util.DoubleUtil;

/**
 * @author waldir
 * Classe serviços - Requerimento e Resposta
 */
@Stateless
public class EquacaoService {
 
	@Inject
	private EquacaoRepository repository; 
	private final int PLACES = 2;
	
	public List<EquacaoEntity> findAll() {
		return repository.findAll();
	}

	public void create(InsertEquacaoRequest request) {
		repository.create(request);
	}
	
	public void update(UpdateEquacaoRequest request) {
		repository.update(request);
	}
	
	public void delete(int id) {
		repository.delete(id);
	}
	/**
	 * @param request
	 * @return
	 */
	public EquacaoResponse calculate(EquacaoRequest request) {

		String message = null;
		
			if (request.a() == 0) {
				return new EquacaoResponse(null, null, null, "Equação invalida! \nCoeficiente \'A\' não pode ser zero (0)");
			}
		
		double delta = calcularDelta(request);
	
			if (delta < 0) {
				message = """
						Quando Delta menor que zero (0)! 
						Não existem raízes.
						""";
				return new EquacaoResponse(delta, null, null, message);
			}
		
		double x1 = calcularX1(request);
		double x2 = calcularX2(request);
 
			if (delta == 0) {
				message = """
						Quando Delta igual a zero (0)!
						O conjunto solução terá valores iguais para X1 e X2.
						Valores encontrados com SUCESSO.
						""";
	
			} else if ((x1 != 0) || (x2 != 0)) {
				message = "Encontrados com SUCESSO.";
			}

			if ((request.b() == 0) || (request.c() == 0)) {
				message = "Equação do 2º Grau INCOMPLETA. \nPorém, VALIDA!";
			}

			EquacaoResponse response = new EquacaoResponse(delta, x1, x2, message);
			InsertEquacaoRequest insertEquacao = new InsertEquacaoRequest(request.a(), request.b(), request.c(), response.delta(), response.x1(), response.x2(), response.message());
			repository.create(insertEquacao);
			
			return response;
	}

		public double calcularDelta(EquacaoRequest request) {
			return DoubleUtil.decimalPlaces(Math.pow(request.b(), 2) - 4 * request.a() * request.c(), PLACES);
		}

		public double calcularX1(EquacaoRequest request) {
			double delta = calcularDelta(request);
			return DoubleUtil.decimalPlaces((-request.b() + Math.sqrt(delta)) / (2 * request.a()), PLACES);
		}

		public double calcularX2(EquacaoRequest request) {
			double delta = calcularDelta(request);
			return DoubleUtil.decimalPlaces((-request.b() - Math.sqrt(delta)) / (2 * request.a()), PLACES);
	}
}