package whphw.com.equacao.dto.request;
//Classe record (esta classe também é chamada de FINAL).
public record UpdateEquacaoRequest(Double a, Double b, Double c, 
		Double delta, Double x1, Double x2, Integer id, String message) {}
