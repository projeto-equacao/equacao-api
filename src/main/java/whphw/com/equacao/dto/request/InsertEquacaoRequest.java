package whphw.com.equacao.dto.request;

public record InsertEquacaoRequest(	Double a, Double b, Double c, 
		                            Double delta, Double x1, Double x2, String message) {}
