package whphw.com.equacao.dto.request;

public record EquacaoRequest(double a, double b, double c) {}
