package whphw.com.equacao.dto.response;

public record EquacaoResponse(Double delta, Double x1, Double x2, String message) {}
