package whphw.com.equacao.dto.response;

public record MessageResponse(String message) {}
