package whphw.com.equacao.repository.jdbc;

import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import whphw.com.equacao.dto.request.InsertEquacaoRequest;
import whphw.com.equacao.dto.request.UpdateEquacaoRequest;
import whphw.com.equacao.entity.EquacaoEntity;

@Stateless
public class EquacaoRepository {

	@PersistenceContext
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	public List<EquacaoEntity> findAll() {
		return manager.createNativeQuery("SELECT * FROM equacao", EquacaoEntity.class)
			   .getResultList();
	} 
	
	public void create(InsertEquacaoRequest request) {
		manager.createNativeQuery("INSERT INTO equacao (a, b, c, delta, x1, x2, message) "
		+ "values (:a, :b, :c, :delta, :x1, :x2, :message)", EquacaoEntity.class)
		.setParameter("a", request.a())
		.setParameter("b", request.b())
		.setParameter("c", request.c())
		.setParameter("delta", request.delta())
		.setParameter("x1", request.x1())
		.setParameter("x2", request.x2())
		.setParameter("message", request.message())
		.executeUpdate();
	}
	
	public void update(UpdateEquacaoRequest request) {
		manager.createNativeQuery("UPDATE equacao SET a=:a, b=:b, c=:c, delta=:delta, x1=:x1, x2=:x2,"
		+ "message=:message WHERE id=:id", EquacaoEntity.class)
		.setParameter("id", request.id())
		.setParameter("a", request.a())
		.setParameter("b", request.b())
		.setParameter("c", request.c())
		.setParameter("delta", request.delta())
		.setParameter("x1", request.x1())
		.setParameter("x2", request.x2())
		.setParameter("message", request.message())
		.executeUpdate();
	}
	
	public void delete(int id) {
		manager.createNativeQuery("DELETE FROM equacao WHERE id = :id", EquacaoEntity.class)
		.setParameter("id", id)
		.executeUpdate();
	}
}
