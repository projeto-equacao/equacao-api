package whphw.com.equacao.controller;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

import jakarta.inject.Inject;

import whphw.com.equacao.dto.request.EquacaoRequest;
import whphw.com.equacao.dto.request.InsertEquacaoRequest;
import whphw.com.equacao.dto.request.UpdateEquacaoRequest;
import whphw.com.equacao.dto.response.EquacaoResponse;
import whphw.com.equacao.dto.response.MessageResponse;
import whphw.com.equacao.entity.EquacaoEntity;
import whphw.com.equacao.service.EquacaoService;

@Path("equacao")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EquacaoController {

	@Inject
	private EquacaoService service;
	
	@GET
	public List<EquacaoEntity> findAll() {
		return service.findAll();
	}
	
	@POST
	@Path("/")
	public EquacaoResponse calculate(EquacaoRequest request) {
		return service.calculate(request);
	}
	
	@POST
	@Path("/cadastrar")
	public void create(InsertEquacaoRequest request) {
		service.create(request);
	}
	
	@PUT
	@Path("{id}")
	public MessageResponse update(UpdateEquacaoRequest request) {
		service.update(request);
		return new MessageResponse("Equação atualizada com sucesso.");
	}

	@DELETE
	@Path("{id}")
	public MessageResponse delete(@PathParam("id") int id){
		service.delete(id);
		return new MessageResponse("Equação removida com sucesso.");
	}
	
}
