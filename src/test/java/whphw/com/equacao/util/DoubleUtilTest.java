package whphw.com.equacao.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DoubleUtilTest {
 
	@Test
	void decimalRoudedUP() {
		double decimal = 20.961463;
		int places = 4;
		double responseQtdePlaces = DoubleUtil.decimalPlaces(decimal, places);
		assertEquals(20.9615, responseQtdePlaces);
	}
 	
	@Test
	void decimalPlacesKeep() {
		double decimal = 3.14541;
		int places = 2;
		
		double responseQtdePlaces = DoubleUtil.decimalPlaces(decimal, places);
		assertEquals(3.14, responseQtdePlaces);
		
	}
	
	@Test
	void decimalPlacesDown() {
		double decimal = 1.2351987;
		int places = 3;
		
		double responseQtdePlaces = DoubleUtil.decimalPlaces(decimal, places);
		assertEquals(1.235, responseQtdePlaces);
		
	}
}
