package whphw.com.equacao.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import whphw.com.equacao.dto.request.EquacaoRequest;
import whphw.com.equacao.dto.response.EquacaoResponse;

@ExtendWith(MockitoExtension.class)
public class EquacaoServiceTest {

	@InjectMocks // instancia automaticamente a variavel
	private EquacaoService service;

	@Test
	void deveRetornarEquacaoInvalidaQuandoAIgualZero() {
		EquacaoRequest requestDeltaMenor = new EquacaoRequest(0, 1, 2);
		EquacaoResponse response = service.calculate(requestDeltaMenor);
		
		assertEquals(null, response.delta());
		assertEquals(null, response.x1());
		assertEquals(null, response.x2());
		assertEquals("Equação invalida! Coeficiente \'A\' não pode ser zero (0)", response.message());
	}

	@Test
	void deveCalcularEquacaoCorretamenteNumerosPositivos() {
		EquacaoRequest requestDeltaMenor = new EquacaoRequest(1, 4, 60);
		EquacaoResponse response = service.calculate(requestDeltaMenor);

		assertTrue(response.delta() < 0);
		assertEquals(null, response.x1());
		assertEquals(null, response.x2());
		assertEquals("""
				Quando Delta menor que zero (0)! Não existem raízes.
				""", response.message());
	}

	@Test
	void deveCalcularEquacaoCorretamenteNumerosNegativos() {
		EquacaoRequest requestDeltaMenor = new EquacaoRequest(-2, -4, -6);
		EquacaoResponse response = service.calculate(requestDeltaMenor);

		assertTrue(response.delta() < 0);
		assertEquals(null, response.x1());
		assertEquals(null, response.x2());
		assertEquals("""
				Quando Delta menor que zero (0)! Não existem raízes.
				""", response.message());
	}
	
	@Test
	void quandoDeltaIgualZeroRaizesIguais() {
		EquacaoRequest request = new EquacaoRequest(1, -6, 9);
		EquacaoResponse responseDeltaIgualZero = service.calculate(request);

		assertEquals(0, responseDeltaIgualZero.delta());
		assertEquals(3, responseDeltaIgualZero.x1());
		assertEquals(3, responseDeltaIgualZero.x2());
		assertEquals("""
				Quando Delta igual a zero (0)!
				O conjunto solução terá valores iguais para X1 e X2.
				Valores encontrados com SUCESSO.
				""", responseDeltaIgualZero.message());
	}
 	
	@Test
	void valorDeCIgualZero() {
		EquacaoRequest requestCZero = new EquacaoRequest(1, -10, 0);
		assertTrue(requestCZero.c() == 0);

		EquacaoResponse ResponseValorDeCIgualZero = service.calculate(requestCZero);
		assertEquals(100, ResponseValorDeCIgualZero.delta());
		assertEquals(10, ResponseValorDeCIgualZero.x1());
		assertEquals(0, ResponseValorDeCIgualZero.x2());
		assertEquals("Equação do 2º Grau INCOMPLETA. Porém, VALIDA!", ResponseValorDeCIgualZero.message());
	}

	@Test
	void valorDeBIgualZero() {
		EquacaoRequest requestBZero = new EquacaoRequest(9, 0, -10);
		assertTrue(requestBZero.b() == 0);

		EquacaoResponse responseValorDeBIgualZero = service.calculate(requestBZero);
		assertEquals(360, responseValorDeBIgualZero.delta());
		assertEquals(1.05, responseValorDeBIgualZero.x1());
		assertEquals(-1.05, responseValorDeBIgualZero.x2());
		assertEquals("Equação do 2º Grau INCOMPLETA. Porém, VALIDA!", responseValorDeBIgualZero.message());

	}
	
	@Test
	void calcularX1() {
		EquacaoRequest request = new EquacaoRequest(7, -5, -3);
		EquacaoResponse responseCalculandoX1 = service.calculate(request);
		assertEquals(109, responseCalculandoX1.delta());
		
		assertEquals(1.1, responseCalculandoX1.x1());
		assertEquals(-0.39, responseCalculandoX1.x2());
	}
	
	@Test
	void calcularX2() {
		EquacaoRequest request = new EquacaoRequest(9, -6, -1);
		EquacaoResponse responseCalculandoX2 = service.calculate(request);
		assertEquals(72, responseCalculandoX2.delta());
		assertEquals(0.8, responseCalculandoX2.x1());

		assertEquals(-0.14, responseCalculandoX2.x2());
	}
}